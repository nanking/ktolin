package com.ctrip.corp.bi

import org.apache.spark.SparkConf
import org.apache.spark.api.java.JavaRDD
import org.apache.spark.api.java.JavaSparkContext
import scala.Tuple2

object Kspark {
    @JvmStatic
    fun main(args: Array<String>) {
        val conf = SparkConf().setAppName("wordCount").setMaster("local")
        val sc = JavaSparkContext(conf)
        val input: JavaRDD<String>? = sc.textFile("D:\\Users\\wang.zheng\\Desktop\\ttt.txt")
        val words = input!!.flatMap { elem -> (elem.split(" ")).iterator() }
            .mapToPair { elem -> Tuple2(elem, 1) }
            .reduceByKey { a, b -> a!! + b!! }
        words.foreach { println(it)}
    }
}